package projet.devops;

//import static java.lang.Thread.sleep;

import java.util.Scanner;

public class main {

	public static void main(String[] args) throws Exception {
	    //throws Exception
		// System.out.println("");
		
		grosBlocEtoiles();

		intro();

		String stationVoulue = demanderLaStation(); //"st-roch";//
		int ligneTAMVoulue = demanderLaLigne(); //0;//

		download.downloadFile("http://data.montpellier3m.fr/node/10732/download", null);
		fichier.analyserLeFichier(stationVoulue, ligneTAMVoulue);
		//sleep(100);
		fichier.supprimerLeFichier();
		grosBlocEtoiles();
	}

	public static String demanderLaStation() {
		System.out.println();
		System.out.print("Saisir le nom de la station recherchée (par exemple 'aiguelongue') : ");
		Scanner clavier = new Scanner(System.in);
		String stationVoulue = clavier.nextLine();
		return stationVoulue;
	}

	public static int demanderLaLigne() {
		System.out.println();
		System.out.print("Saisir le numéro de la ligne recherche (saisir 0 si vous ne savez pas) : ");
		Scanner clavier = new Scanner(System.in);
		int ligneVoulue = clavier.nextInt();
		return ligneVoulue;
	}

	public static void intro() {
		System.out.println();
		System.out.println("Bienvenue sur l'application HORAIRES TAM en temps réel !");
		System.out.println(
				"Cette application vous permet de connaitre le prochain passage d'un Tramway ou d'un BUS en spécifiant la station souhaitée !");
	}
	
	public static void grosBlocEtoiles() {
		System.out.println();
		System.out.println("**********************************************************************");
		System.out.println("**********************************************************************");
		System.out.println();
	}

}
