package projet.devops;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class download {

	public static void downloadFile(String adresse, File dest) {
		BufferedReader reader = null;
		FileOutputStream fos = null;
		InputStream in = null;
		try {

			// cr�ation de la connection
			URL url = new URL(adresse);
			URLConnection conn = url.openConnection();
			// System.out.println(adresse);

			String FileType = conn.getContentType();
			// System.out.println("FileType : " + FileType);

			int FileLenght = conn.getContentLength();
			if (FileLenght == -1) {
				throw new IOException("Fichier non valide.");
			}

			// lecture de la r�ponse
			in = conn.getInputStream();
			reader = new BufferedReader(new InputStreamReader(in));
			if (dest == null) {
				String FileName = url.getFile();
				FileName = FileName.substring(FileName.lastIndexOf('/') + 1);
				dest = new File(FileName);
			}
			fos = new FileOutputStream(dest + ".csv");
			// System.out.println(dest);
			// System.out.println();

			byte[] buff = new byte[1024];
			int l = in.read(buff);
			while (l > 0) {
				fos.write(buff, 0, l);
				l = in.read(buff);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fos.flush();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				reader.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
