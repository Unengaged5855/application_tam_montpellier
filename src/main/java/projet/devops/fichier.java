package projet.devops;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class fichier {

	public static void lireLeFichier() {
		try {
			BufferedReader br = new BufferedReader(new FileReader("download.csv"));

			String ligne = null;
			while ((ligne = br.readLine()) != null) {

				String[] data = ligne.split(";");

				for (String val : data) {
					System.out.print(val + " // ");

				}
				System.out.println();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void analyserLeFichier(String stationVoulue, int ligneVoulue) {

		try {
			String chaineFormatee = "| %23s | %20s | %20s | %16s |";
			boolean imprimerEntete = false;
			////

			BufferedReader br = new BufferedReader(new FileReader("download.csv"));

			String ligne = null;
			while ((ligne = br.readLine()) != null) {

				String[] data = ligne.split(";");
				/*
				 * for (String val : data) {
				 * 
				 * //data[3] = nom de station // data[4] = numero de ligne // data[5] =
				 * direction // data[7] =prochain passage }
				 */
				if (direSiLaStationExiste(data[3], stationVoulue) == true) {
					if (ligneVoulue == 0) {
						imprimerEntete = imprimerEntete(chaineFormatee, imprimerEntete);
						imprimerDonnees(chaineFormatee, data[4], data[3], data[5], data[7]);

					} //////////////////////
					else if (direSiLaLigneTAMExiste(data[4], ligneVoulue) == true) {
						imprimerEntete = imprimerEntete(chaineFormatee, imprimerEntete);
						imprimerDonnees(chaineFormatee, data[4], data[3], data[5], data[7]);
					}
				}

			}
			if (imprimerEntete == false) {
				System.out.println("****************************************************");
				System.out.println("DESOLE, AUCUNE DONNEE NE CORRESPOND A VOS CRITERES !");
				System.out.println("****************************************************");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void supprimerLeFichier() {
		try {

			File file = new File("download.csv");

			if (file.delete()) {
				// System.out.println(file.getName() + " est supprim�.");
			} else {
				// System.out.println("Op�ration de suppression echou�e");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static boolean direSiLaStationExiste(String stationActuelle, String stationVoulue) {

		stationVoulue = stationVoulue.toUpperCase();
		// System.out.println(stationVoulue);
		String[] data = stationVoulue.split(" ");
		// Pour chaque mot de la recherche, rechercher s'il existe
		boolean laStationExiste = false;
		for (String chaqueMot : data) {
			// System.out.print(chaqueMot + " // ");
			// data[3] = nom de station // data[4] = numero de ligne // data[5] = direction
			// // data[7] =prochain passage
			int intIndex = stationActuelle.indexOf(chaqueMot);
			if (intIndex != -1) {
				laStationExiste = true;
			}
		}

		return laStationExiste;
	}

	public static boolean direSiLaLigneTAMExiste(String ligneTAMactuelle, int ligneTAMvoulue) {
		int laLigneTAMactuelle = Integer.parseInt(ligneTAMactuelle);

		boolean laLigneTAMexiste = false;
		if (laLigneTAMactuelle == ligneTAMvoulue) {
			laLigneTAMexiste = true;
		}

		return laLigneTAMexiste;
	}

	public static boolean imprimerEntete(String chaineFormatee, boolean imprimerEntete) {
		if (imprimerEntete == false) {
			System.out.println();
			System.out.println(String.format(chaineFormatee, "Numero de ligne", "Nom de la station", "Direction",
					"Prochain passage"));
			System.out.println(
					"|-------------------------+----------------------+----------------------+------------------|");
		}

		return true;
	}

	public static void imprimerDonnees(String chaineFormatee, String numeroLigne, String nomStation, String direction,
			String prochainPassage) {
		// data[3] = nom de station // data[4] = numero de ligne // data[5] = direction
		// // data[7] =prochain passage
		System.out.println(
				String.format(chaineFormatee, "Ligne " + numeroLigne + " de " + identiferTRAMouBUS(numeroLigne),
						nomStation, direction, prochainPassage));

	}

	public static String identiferTRAMouBUS(String numeroDeLigne) {
		String typeLigne = "BUS";
		for (int num = 1; num < 5; num++) {
			if (num == Integer.parseInt(numeroDeLigne)) {
				typeLigne = "TRAMWAY";
			}
		}
		return typeLigne;
	}

}
